package ru.nekki.directorymonitor.util;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by dima on 07.10.2016.
 */
public class TestPropertiesHolder extends PropertiesHolder {

    public TestPropertiesHolder(String... args) {
        super(args);
        loadTestSet();
    }

    private void loadTestSet() {
        URI uri = null;
        try {
            uri = TestPropertiesHolder.class.getResource("testset").toURI();
        } catch (URISyntaxException e) {
            LOG.error("Retrieving test set directory path failed" , e);
        }

        if (uri != null) {
            appProperties.put("default.app.testSetDir", Paths.get(uri).toString());
        }
    }

    protected Path getPathProperty(String property) {
        try {
            return Files.createTempDirectory(runningAppPath(), property);
        } catch (IOException e) {
            LOG.error("Creating directory {} failed. Exception message: {}" , runningAppPath(), e.getMessage());
        }
        return null;
    }

}