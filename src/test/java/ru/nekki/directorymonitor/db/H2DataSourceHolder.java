package ru.nekki.directorymonitor.db;

import org.h2.jdbcx.JdbcDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by dima on 06.10.2016.
 */
public class H2DataSourceHolder implements DataSourceHolder {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    private JdbcDataSource dataSource;

    public H2DataSourceHolder() {
        createDataSource();
        createTableIfNeeded();
    }

    private void createDataSource() {
        dataSource = new JdbcDataSource();
        dataSource.setURL("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        dataSource.setUser("sa");
        dataSource.setPassword("sa");
    }

    private void createTableIfNeeded() {
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
        ) {
            statement.execute(
                    "create TABLE IF NOT EXISTS entry (\n" +
                            "  id int auto_increment primary key,\n" +
                            "  content varchar(1024) not null,\n" +
                            "  creationdate TIMESTAMP not null,\n" +
                            "  filename varchar not null\n" +
                            ");\n");
        } catch (SQLException e) {
            LOG.error("Table creation failed.", e);
        }
    }

    public DataSource getDataSource() {
        return dataSource;
    }
}