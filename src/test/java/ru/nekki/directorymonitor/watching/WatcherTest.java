package ru.nekki.directorymonitor.watching;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nekki.directorymonitor.db.DataSourceHolder;
import ru.nekki.directorymonitor.db.H2DataSourceHolder;
import ru.nekki.directorymonitor.util.CopyFileVisitor;
import ru.nekki.directorymonitor.util.DeleteFileVisitor;
import ru.nekki.directorymonitor.util.TestPropertiesHolder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by dima on 07.10.2016.
 */
public class WatcherTest {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    private DataSourceHolder dataSourceHolder;
    private TestPropertiesHolder testPropertiesHolder;
    private WatcherParams watcherParams;

    @Before
    public void setUp() throws Exception {
        dataSourceHolder = new H2DataSourceHolder();
        testPropertiesHolder = new TestPropertiesHolder();
        watcherParams = testPropertiesHolder.getParams();

        prepareWatchingDir();
    }

    @After
    public void tearDown() throws Exception {
        Files.walkFileTree(watcherParams.getWatching(), new DeleteFileVisitor());
        Files.walkFileTree(watcherParams.getProcessed(), new DeleteFileVisitor());
        Files.walkFileTree(watcherParams.getUnprocessed(), new DeleteFileVisitor());
    }

    private void prepareWatchingDir() throws IOException {
        Path source = Paths.get(testPropertiesHolder.getProperty("testSetDir"));
        Path target = watcherParams.getWatching();

        try {
            Files.walkFileTree(source, new CopyFileVisitor(target));
        } catch (IOException e) {
            LOG.error("Failed to copy test file set to watching path. Aborting...", e);
            throw e;
        }
        LOG.info("Test file set is ready.");
    }

    @Test
    public void testDatabaseConsistency() throws Exception {
        startWatcherForOneSecond();

        List<String> actual = actualFilesInDatabase();
        List<String> expected = Arrays.asList("1.xml","11","12.csv","2.xml","5.xml");
        assertThat(actual, is(expected));
    }

    @Test
    public void testWatchingDir() throws Exception {
        startWatcherForOneSecond();

        long watchingDirFiles = Files.list(watcherParams.getWatching()).count();
        assertThat(watchingDirFiles, is(0L));
    }

    @Test
    public void testProcessedDir() throws Exception {
        startWatcherForOneSecond();

        List<String> actualProcessed = Files.list(watcherParams.getProcessed()).map(Path::getFileName).map(Path::toString).collect(Collectors.toList());
        List<String> expectedProcessed = Arrays.asList("1.xml","11","12.csv","2.xml","5.xml");
        assertThat(actualProcessed, is(expectedProcessed));
    }

    @Test
    public void testUnprocessedDir() throws Exception {
        startWatcherForOneSecond();

        List<String> actualUnprocessed = Files.list(watcherParams.getUnprocessed()).map(Path::getFileName).map(Path::toString).collect(Collectors.toList());
        List<String> expectedUnprocessed = Arrays.asList("10-failed.xml", "3-failed.xml", "4-failed.xml", "6-failed.xml", "7-failed.xml", "8-failed.xml", "9-failed.xml", "testset.lnk");
        assertThat(actualUnprocessed, is(expectedUnprocessed));
    }

    private void startWatcherForOneSecond() throws Exception {
        Watcher watcher = new Watcher(watcherParams, dataSourceHolder.getDataSource());

        watcher.start();
        Thread.sleep(1000);
        watcher.stop(false);
    }

    private List<String> actualFilesInDatabase() {
        List<String> files = new ArrayList<>();
        try (
                Connection connection = dataSourceHolder.getDataSource().getConnection();
                Statement statement = connection.createStatement();
        ) {
            statement.execute("SELECT * from entry;");
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                files.add(resultSet.getString("filename"));
            }
        } catch (SQLException e) {
            LOG.error("Selecting from table failed.", e);
        }

        Collections.sort(files);
        return files;
    }
}