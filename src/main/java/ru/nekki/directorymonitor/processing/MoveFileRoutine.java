package ru.nekki.directorymonitor.processing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nekki.directorymonitor.util.MoveDirectoryVisitor;
import ru.nekki.directorymonitor.watching.WatcherParams;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.EnumSet;

/**
 * Created by dima on 06.10.2016.
 */
public class MoveFileRoutine implements Runnable {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    private Path source;
    private Path target;

    public MoveFileRoutine(Path source, WatcherParams params, boolean correctlyProcessed) {
        this.source = source;
        this.target = Paths.get(correctlyProcessed ? params.getProcessed().toString() : params.getUnprocessed().toString(), source.getFileName().toString());
    }

    @Override
    public void run() {
        if (!Files.isReadable(source) || !Files.exists(source)) {
            LOG.info("Can't move file {}.", source);
            return;
        }

        try {
            if (Files.isDirectory(source) && Files.list(source).count() > 0) {
                Files.walkFileTree(source, EnumSet.of(FileVisitOption.FOLLOW_LINKS), Integer.MAX_VALUE, new MoveDirectoryVisitor(target));
            } else {
                Files.move(source, target, StandardCopyOption.REPLACE_EXISTING);
            }
            LOG.info("File {} was successfully processed to {}.", source, target);
        } catch (IOException e) {
            LOG.error("Moving {} to {} failed. Exception: ", source, target, e);
        }
    }
}
