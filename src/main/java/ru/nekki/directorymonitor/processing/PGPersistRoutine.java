package ru.nekki.directorymonitor.processing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nekki.directorymonitor.model.Entry;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.concurrent.Callable;

/**
 * Created by dima on 06.10.2016.
 */
public class PGPersistRoutine implements Callable<Boolean> {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    private final DataSource dataSource;
    private final Entry entry;

    public PGPersistRoutine(Entry entry, DataSource dataSource) {
        this.entry = entry;
        this.dataSource = dataSource;
    }

    @Override
    public Boolean call() {
        return updateDatabase();
    }

    /**
     * Database update method: inserts a row per <code>Entry</code> instance,
     * if the database table was not contain row for this file before.
     * @return <code>true</code> if update was successful.
     */
    private boolean updateDatabase() {
        if (entryExists()) {
            return true;
        }

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement("INSERT INTO entry (content, creationDate, filename) VALUES (?, ?, ?);")) {
            statement.setString(1, entry.getContent());
            statement.setTimestamp(2, new Timestamp(entry.getCreationDate().getTime()));
            statement.setString(3, entry.getFileName());
            statement.execute();
        } catch (SQLException e) {
            LOG.error("Connection error occurred.", e);
            return false;
        }
        return true;
    }

    private boolean entryExists() {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM entry WHERE filename = ?;")) {
            statement.setString(1, entry.getFileName());
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            LOG.error("Connection error occurred.", e);
        }
        return false;
    }
}
