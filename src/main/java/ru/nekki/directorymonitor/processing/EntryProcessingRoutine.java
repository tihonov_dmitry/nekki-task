package ru.nekki.directorymonitor.processing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import ru.nekki.directorymonitor.model.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by dima on 06.10.2016.
 */
public class EntryProcessingRoutine implements Callable<Entry> {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    private Path file;

    public EntryProcessingRoutine(Path file) {
        this.file = file;
    }

    /**
     * Validates XML-file and performs mapping to object, that was passed through constructor param.
     * @return  valid <code>Entry</code> instance, if parsing was successful, <code>null</code> otherwise.
     */
    @Override
    public Entry call() {
        return isValid() ? parseFile() : null;
    }

    private boolean isValid() {
        List<String> expressions = Arrays.asList("/Entry/creationDate", "/Entry/content");
        XPath xpath = XPathFactory.newInstance().newXPath();
        return !Files.isDirectory(file) && !isCyclicSymbolicLink(file) && expressions.stream().allMatch(expression -> containsNode(xpath, expression));
    }

    private boolean containsNode(XPath xpath, String xPathExpr) {
        NodeList nodes = null;
        try (InputStream is = new FileInputStream(file.toFile())) {
            InputSource inputSource = new InputSource(is);
            nodes = (NodeList) xpath.evaluate(xPathExpr, inputSource, XPathConstants.NODESET);
        } catch (XPathExpressionException | IOException e) {
            LOG.error("Error during validation process occurred for file {}.", file);
            LOG.error("Detailed message: {}", e.getMessage());
            return false;
        }

        return nodes != null && nodes.getLength() == 1;
    }

    private Entry parseFile() {
        Entry result = null;

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Entry.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            result = (Entry) jaxbUnmarshaller.unmarshal(file.toFile());
        } catch (JAXBException e) {
            LOG.error("Parsing file error occurred.", e);
        }

        if (result != null) {
            result.setFileName(file.getFileName().toString());
        }

        return result;
    }

    private boolean isCyclicSymbolicLink(Path file) {
        if (!Files.isSymbolicLink(file)) {
            return false;
        }

        Path hare = readSymbolicLink(file);
        Path tortoise = readSymbolicLink(file);

        if (hare == null) {
            // if error occurred, better mark file as cycled link and count as unprocessed - it's safer
            LOG.warn("Error occurred during finding cycles on symbolic link. {} will be marked as unprocessed. ", file);
            return true;
        }

        while (Files.isSymbolicLink(hare)) {
            hare = readSymbolicLink(hare);

            if (hare == null) {
                LOG.info("Error occurred during finding cycles on symbolic link. {} will be marked as unprocessed. ", file);
                return true;
            }

            if (!Files.isSymbolicLink(hare)) {
                return false;
            }

            hare = readSymbolicLink(hare);
            tortoise = readSymbolicLink(tortoise);

            if (hare == null) {
                LOG.warn("Error occurred during finding cycles on symbolic link. {} will be marked as unprocessed. ", file);
                return true;
            }

            if (hare.equals(tortoise)) {
                LOG.info("{} is a cyclic symbolic link.", file);
                return true;
            }
        }

        return false;
    }

    private Path readSymbolicLink(Path symlink) {
        try {
            Path result = Files.readSymbolicLink(symlink);

            if (result.isAbsolute()) {
                return result;
            } else {
                return symlink.getParent().resolve(result);
            }
        } catch (IOException e) {
            LOG.error("Reading symbolic link error occurred.", e);
        }

        return null;
    }


}
