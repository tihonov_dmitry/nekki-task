package ru.nekki.directorymonitor.processing;

import ru.nekki.directorymonitor.model.Entry;
import ru.nekki.directorymonitor.watching.WatcherParams;

import javax.sql.DataSource;
import java.nio.file.Path;

/**
 * Created by dima on 04.10.2016.
 */
public class FileProcessRoutine implements Runnable {

    private Path file;
    private WatcherParams params;
    private DataSource dataSource;

    public FileProcessRoutine(Path file, WatcherParams params, DataSource dataSource) {
        this.file = file;
        this.params = params;
        this.dataSource = dataSource;
    }

    @Override
    public void run() {
        Entry entry = new EntryProcessingRoutine(file).call();
        if (entry != null) {
            Boolean persisted  = new PGPersistRoutine(entry, dataSource).call();

            if (persisted != null && persisted) {
                new MoveFileRoutine(file, params, true).run();
            }
        } else {
            new MoveFileRoutine(file, params, false).run();
        }
    }

}
