package ru.nekki.directorymonitor.watching;

import java.nio.file.Path;

/**
 * Created by dima on 06.10.2016.
 */
public class WatcherParams {

    private final Path watching;
    private final Path processed;
    private final Path unprocessed;
    private final int monitoringPeriod;

    public WatcherParams(Path watching, Path processed, Path unprocessed, int monitoringPeriod) {
        this.watching = watching;
        this.processed = processed;
        this.unprocessed = unprocessed;
        this.monitoringPeriod = monitoringPeriod;
    }

    public Path getWatching() {
        return watching;
    }

    public Path getProcessed() {
        return processed;
    }

    public Path getUnprocessed() {
        return unprocessed;
    }

    public int getMonitoringPeriod() {
        return monitoringPeriod;
    }

}
