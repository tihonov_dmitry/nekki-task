package ru.nekki.directorymonitor.watching;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nekki.directorymonitor.processing.FileProcessRoutine;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by dima on 04.10.2016.
 */
public class Watcher {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    private final long MAX_TASKS = 10L;

    private ExecutorService routineExecutorService = Executors.newWorkStealingPool();
    private ScheduledExecutorService watcherExecutor = Executors.newSingleThreadScheduledExecutor();

    private ConcurrentMap<String, Future> tasks = new ConcurrentHashMap<>();

    private WatcherParams params;
    private DataSource dataSource;

    private boolean isRunning;

    public Watcher(WatcherParams params, DataSource dataSource) {
        this.params = params;
        this.dataSource = dataSource;
    }

    public void start() {
        if (dataSource == null) {
            LOG.error("Data source is null. Aborting...");
            return;
        }

        if (params == null) {
            LOG.error("Watcher params is null. Aborting...");
            return;
        }

        if (isRunning) {
            LOG.warn("Watcher is already running. Method call is aborted.");
            return;
        }

        isRunning = true;
        watcherExecutor.scheduleAtFixedRate(iterateWatchingRunnable, 0, params.getMonitoringPeriod(), TimeUnit.MILLISECONDS);
    }

    public void stop(boolean force) {
        if (force) {
            LOG.info("Application will be forcibly stopped right now.");
            watcherExecutor.shutdownNow();
            routineExecutorService.shutdownNow();
        } else {
            LOG.info("Application will be stopped.");
            watcherExecutor.shutdown();
            routineExecutorService.shutdown();
        }
    }

    private Runnable iterateWatchingRunnable = () -> {
        tasks.forEach((s, future) -> {
            if (future.isDone()) {
                tasks.remove(s, future);
            }
        });

        if (tasks.size() >= MAX_TASKS) {
            return;
        }

        List<Path> watchingFiles = null;
        try {
            watchingFiles = Files.list(params.getWatching()).limit(MAX_TASKS).collect(Collectors.toList());
        } catch (IOException e) {
            LOG.error("Error retrieving file list in watching directory. Aborting...", e);
            System.exit(0);
            return;
        }

        if (watchingFiles == null) {
            LOG.error("File list in watching directory cannot be initialized for some reason. Aborting...");
            System.exit(0);
            return;
        }

        watchingFiles.forEach(file -> {
            String fileName = file.getFileName().toString();
            Future existingTask = tasks.putIfAbsent(fileName, routineExecutorService.submit(new FileProcessRoutine(file, params, dataSource)));
            if (existingTask != null) {
                tasks.replace(fileName, existingTask).cancel(true);
            }
        });
    };

}
