package ru.nekki.directorymonitor.db;

import org.postgresql.ds.PGPoolingDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nekki.directorymonitor.util.PropertiesHolder;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by dima on 04.10.2016.
 */
public class PGDataSourceHolder implements DataSourceHolder {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    private PGPoolingDataSource dataSource;
    private PropertiesHolder propertiesHolder;

    public PGDataSourceHolder(PropertiesHolder propertiesHolder) {
        this.propertiesHolder = propertiesHolder;

        createDataSource();
        createTableIfNeeded();
    }

    private void createDataSource() {
        try {
            dataSource = new PGPoolingDataSource();
            dataSource.setUrl(propertiesHolder.getProperty("url"));
            dataSource.setUser(propertiesHolder.getProperty("username"));
            dataSource.setPassword(propertiesHolder.getProperty("password"));
        } catch (Exception e) {
            LOG.error("Data source creation failed.", e);
        }
    }

    private void createTableIfNeeded() {
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
        ) {
            statement.execute(
                    "create TABLE IF NOT EXISTS entry (\n" +
                            "  id SERIAL not null,\n" +
                            "  content varchar(1024) not null,\n" +
                            "  creationdate TIMESTAMP not null,\n" +
                            "  filename varchar not null,\n" +
                            "  PRIMARY KEY (id)\n" +
                            ");\n");
        } catch (SQLException e) {
            LOG.error("Table creation failed.", e);
        }
    }

    public DataSource getDataSource() {
        return dataSource;
    }
}
