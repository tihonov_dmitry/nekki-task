package ru.nekki.directorymonitor.db;

import javax.sql.DataSource;

/**
 * Created by dima on 04.10.2016.
 */
public interface DataSourceHolder {

    DataSource getDataSource();

}
