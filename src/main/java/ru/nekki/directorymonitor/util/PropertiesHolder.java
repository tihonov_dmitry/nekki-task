package ru.nekki.directorymonitor.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nekki.directorymonitor.watching.WatcherParams;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by dima on 06.10.2016.
 */
public class PropertiesHolder {

    protected final static Logger LOG = LoggerFactory.getLogger(PropertiesHolder.class);

    protected Properties appProperties = new Properties();

    public PropertiesHolder(String... args) {
        loadDefaults();

        for (String arg : args) {
            loadProperties(appProperties, Paths.get(arg).normalize());
        }
    }

    public static void loadProperties(Properties properties, Path propertyPath) {
        if (propertyPath == null) {
            LOG.warn("Properties path is null. Aborting...");
            return;
        }

        if (!Files.exists(propertyPath) || Files.notExists(propertyPath)) {
            LOG.info("File {} not found.", propertyPath);
            return;
        }

        try (InputStream inputStream = new FileInputStream(propertyPath.toFile())) {
            properties.load(inputStream);
        } catch (IOException e) {
            LOG.error("Properties loading is failed.", e);
        }
    }

    private void loadDefaults() {
        URL url = ClassLoader.getSystemResource("META-INF/default.properties");
        if (url != null) {
            try {
                appProperties.load(url.openStream());
            } catch (IOException e) {
                LOG.error("Loading default properties failed" , e);
            }
        }
    }

    public String getProperty(String property) {
        Path appPath = runningAppPath();

        String defaults = appProperties.getProperty("default.app." + property);
        if (defaults != null && appPath != null) {
            defaults = defaults.replace("${appPath}", appPath.toString());
        }

        String specified = appProperties.getProperty("app." + property);
        if (specified != null && appPath != null) {
            specified = specified.replace("${appPath}", appPath.toString());
        }

        if (specified == null || specified.isEmpty()) {
            LOG.info("Property 'app.{}' not specified - using default value: {}", property, defaults);
        }
        return specified == null || specified.isEmpty() ? defaults : specified;
    }

    public WatcherParams getParams() {
        Path watching = getPathProperty("watchingPath");
        Path processed = getPathProperty("processedPath");
        Path unprocessed = getPathProperty("unprocessedPath");
        int monitoringPeriod = Integer.parseInt(getProperty("monitoringPeriod"));

        return new WatcherParams(watching, processed, unprocessed, monitoringPeriod);
    }

    protected Path getPathProperty(String property) {
        String stringProperty = getProperty(property);
        Path path = Paths.get(stringProperty);
        createDirIfNotExist(path);

        if (!Files.isDirectory(path)) {
            throw new IllegalArgumentException(String.format("Specified path '%s' is not a directory. Application aborting...", path));
        }

        return path;
    }

    protected Path runningAppPath() {
        Path appPath = null;
        try {
            appPath = Paths.get(getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
        } catch (URISyntaxException e) {
            LOG.error("Retrieving running app path is failed.", e);
        }

        return appPath != null && Files.isDirectory(appPath) ? appPath : appPath.getParent();
    }

    private void createDirIfNotExist(Path path) {
        if (!Files.exists(path)) {
            try {
                Files.createDirectory(path);
            } catch (IOException e) {
                LOG.error("Creating directory {} failed. Exception message: {}" , path, e.getMessage());
            }
        }
    }

}
