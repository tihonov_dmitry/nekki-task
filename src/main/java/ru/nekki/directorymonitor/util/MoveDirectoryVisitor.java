package ru.nekki.directorymonitor.util;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by dima on 07.10.2016.
 */
public class MoveDirectoryVisitor extends SimpleFileVisitor<Path> {

    private final Path targetPath;
    private Path sourcePath = null;

    public MoveDirectoryVisitor(Path targetPath) {
        this.targetPath = targetPath;
    }

    @Override
    public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
        if (sourcePath == null) {
            sourcePath = dir;
        } else {
            Files.createDirectories(targetPath.resolve(sourcePath.relativize(dir)));
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        if (Files.list(sourcePath).count() == 0L) {
            Files.delete(sourcePath);
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
        if (Files.isDirectory(sourcePath) && (!Files.exists(targetPath) || Files.notExists(targetPath))) {
            Files.createDirectories(targetPath);
        }

        Files.move(file, targetPath.resolve(sourcePath.relativize(file)), StandardCopyOption.REPLACE_EXISTING);

        return FileVisitResult.CONTINUE;
    }
}