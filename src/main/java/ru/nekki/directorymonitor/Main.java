package ru.nekki.directorymonitor;

import ru.nekki.directorymonitor.db.DataSourceHolder;
import ru.nekki.directorymonitor.db.PGDataSourceHolder;
import ru.nekki.directorymonitor.util.PropertiesHolder;
import ru.nekki.directorymonitor.watching.Watcher;

import java.io.IOException;

/**
 * Created by dima on 03.10.2016.
 */
public class Main {

    public static void main(String args[]) {
        PropertiesHolder propertiesHolder = new PropertiesHolder(args);
        DataSourceHolder dataSourceHolder = new PGDataSourceHolder(propertiesHolder);

        Watcher watcher = new Watcher(propertiesHolder.getParams(), dataSourceHolder.getDataSource());

        watcher.start();

        System.out.print("Press ENTER key to terminate application...");
        int key;
        try {
            key = System.in.read();
        } catch (IOException e) {}
        watcher.stop(false);
    }

}
