# Directory monitor  #
Данное приложение предназначено для мониторинга заданной директории с определенной периодичностью на наличие файлов определенного XML-формата. При обнаружении файла подходящего формата приложение сохраняет его содержимое в аналогичную по структуре таблицу в PostgreSQL и перемещает файл в каталог обработанных файлов.

Формат входного XML-файла:
```
<Entry>
    <!--строка длиной до 1024 символов-->
    <content>Содержимое записи</content>
    <!--дата создания записи-->
    <creationDate>2014-01-01 00:00:00</creationDate>
</Entry>
```

#### Сборка ####
Сборка осуществляется средствами Gradle Wrapper.
Для этого необходимо через интерфейс командной строки перейти в директорию `nekkitest`, после чего выполнить в ней команду

`./gradlew build` (на Unix-подобных платформах или Windows PowerShell)

`gradlew build` (в комнадной строке Windows)

В случае успешного завершения сборки, будут созданы zip- и tar-архивы с дистрибутивом приложения, которые будут расположены в директории `nekkitest/build/distributions/`.

#### Запуск ####
Для запуска приложения необходимо:

1) распаковать содержимое любого архива в требуемую директорию `{targetDir}`;

2) задать параметры доступа к PostgreSQL (имя, пароль и URL) в файле `{targetDir}/directory-monitor-1.0/config/connection.properties`, который имеет следующую структуру:
```
app.username={username}
app.password={password}
app.url={url}
```
3) в зависимости от используемой на целевой машине ОС, запустить `{targetDir}/directory-monitor-1.0/bin/app.bat` либо `{targetDir}/directory-monitor-1.0/app/bin/app`.

#### Конфигурирование  ####
Приложение использует следующие параметры по умолчанию:

- директория мониторинга - `${appPath}/watching`;
- директория обработанных файлов - `${appPath}/processed`;
- директория файлов, обработка которых не удалась - `${appPath}/unprocessed`;
- период мониторинга - `500` мс;


где `${appPath}` - путь до исполняемого файла приложения.

Для использования других значений следует отредактировать содержимое файла `{targetDir}/directory-monitor-1.0/config/app.properties`, который имеет следующую стрктуру:
```
app.watchingPath=${appPath}/source
app.processedPath=${appPath}/processed
app.unprocessedPath=${appPath}/unprocessed
app.monitoringPeriod=500
```